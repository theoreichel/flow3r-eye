# Eye
Just one additional eye to look around.

# Installation and usage
Just upload the App in `sys/apps/eye` and enjoy.

Control the eye fatigue (eyelids distance) with the first and third petal (clockwise from USB-C connector).
Trigger a blink with the fifth (bottom) petal.

# TODO
- Fix TODOs: a bit of math and cleanup
- Fall asleep when the badge isn't moving (gyro)
- Plug two badges (or more) together with the mini-jack to synchronize eyes.
  See if it make sense to use asymmetrical eyelids.
- Get afraid when surroundings get loud. (Open the eye more, get stressed)
- Get angry (red eye and led color) when shaken (stressed)
- Do some sound? Let's pretend it's a rusty eye...
- Set iris position from input: network, bluetooth?, mini-jack, sound?

# Credits
- CCCamp (badge teams) https://flow3r.garden/
- Adafruit (Image assets) https://learn.adafruit.com/animated-electronic-eyes/overview

