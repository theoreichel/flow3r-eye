import st3m.run, random, leds
import time
import random

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m.input import InputController
from st3m.utils import tau
from ctx import Context

class AnotherEye(Application):
    def __init__(self, context):
        super().__init__(context)
        self.input = InputController()

        # can be adjusted with the first and third petal (from USB-C connector, clockwise)
        self.lids_spacing = 70 # 240 - this value.
        # Lid course difference between the upped and lower lid. The lower lid moves less. 20% seems to be a good value.
        self.look_up_down_differential = 0.2
        # How fast the eye is blinking, min and max values are required.
        self.blink_delays = [500, 2000]
        # How often the iris is moving and the look changing position. min, max values.
        self.look_change_delays = [1000, 5000]

        # END OF SETTINGS

        leds.set_all_rgb(0, 0, 0)

        self.path      = self.app_ctx.bundle_path + "/"
        # self.path      = "/flash/sys/apps/eye/"
        self.lid_lower = self.path + "lid-lower-symmetrical.png"
        self.lid_upper = self.path + "lid-upper-symmetrical.png"
        self.sclera    = self.path + "sclera-iris.png"

        # Positions
        self.lids = [0,0]
        self.iris = [0,0]

        # Will be computed based on the iris position
        self.opened_lids = [0, 0]
        # Always closes midway
        self.closed_lids = [120, 120]

        # Blinking variables
        self.blinking = False
        self.last_blink_closed_at = 0
        self.last_blink_open_at = 1
        self.next_blink_in = 1000

        # Iris position variables
        self.iris_destination = self.iris
        self.next_look_change_at = 2000
        self.iris_movement_speed = 1

    def draw(self, ctx: Context) -> None:
        ctx.image_smoothing = False

        # Set a background
        ctx.rgb(0,0,0).rectangle(-120, -120, 240, 240).fill()

        # Place eye images, first the globe
        ctx.image(self.sclera, -180 + self.iris[0], -180 + self.iris[1] , 360, 360)

        # Then eyelids
        ctx.rgb(0,0,0).rectangle(-120, -120, 240, self.lids[0] + 1).fill()
        ctx.image(self.lid_upper, -120, -120 + self.lids[0], 240, 120)

        ctx.image(self.lid_lower, -120, -self.lids[1], 240, 120)
        ctx.rgb(0,0,0).rectangle(-120, 120 - self.lids[1] - 1, 240, self.lids[1] + 1).fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        now = time.time_ns() / 1e6

        # Inputs
        # Lids spacing
        if self.input.captouch.petals[1].pressure > 0:
            if self.lids_spacing > 0:
                self.lids_spacing -= 1
                self.blinking = True
        if self.input.captouch.petals[3].pressure > 0:
            if self.lids_spacing < 120:
                self.lids_spacing += 1
                self.blinking = True

        # Just looking (move iris)
        if self.next_look_change_at < now:
            self.next_look_change_at = now + random.randint(*self.look_change_delays)
            self.iris_destination = [random.randint(-50, 50), random.randint(-50, 50)]
            self.iris_movement_speed = random.randint(1,5)

        # Trigger Blink
        if self.last_blink_open_at + self.next_blink_in < now:
            self.next_blink_in = random.randint(*self.blink_delays)
            self.blinking = True

        if self.input.captouch.petals[5].pressure > 0:
            self.blinking = True

        # Compute
        if self.blinking:
            self.process_blink(now)
        else:
            self.process_iris_movement(self.iris_movement_speed)
            self.process_sprites_positions()

        leds.set_all_rgb(0, max(0, 60 - abs(self.iris[0])), max(0, 60 - abs(self.iris[1])))
        leds.update()

    def process_blink(self, now, speed = 10):
        if self.last_blink_closed_at < self.last_blink_open_at:
            if self.close_lids(speed):
                self.last_blink_closed_at = now
                self.iris = [random.randint(-50, 50), random.randint(-50, 50)]
                self.iris_destination = self.iris
        else:
            if self.open_lids(speed):
                self.last_blink_open_at = now
                self.blinking = False

    def close_lids(self, speed = 1):
        for i in range(2):
            self.lids[i] = min(self.closed_lids[i], self.lids[i] + speed)
        return self.lids == self.closed_lids

    def open_lids(self, speed = 1):
        for i in range(2):
            self.lids[i] = max(self.opened_lids[i], self.lids[i] - speed)
        return self.lids == self.opened_lids

    def process_iris_movement(self, speed = 1):
        if self.iris_destination != self.iris:
            distance = [0, 0]
            for i in range(2):
                distance[i] = abs(self.iris_destination[i] - self.iris[i]) / speed
            steps = max(distance)

            for i in range(2):
                if self.iris[i] < self.iris_destination[i]:
                    self.iris[i] += min(distance[i] / steps, distance[i])
                elif self.iris[i] > self.iris_destination[i]:
                    self.iris[i] -= min(distance[i] / steps, distance[i])

    def process_sprites_positions(self):
        half_lid_spacing = self.lids_spacing / 2
        # Only y axis is relevant for lids positions
        # Lids always close midway
        self.lids = [
            self.iris[1] * (1 + self.look_up_down_differential) + half_lid_spacing,
            - self.iris[1] / (1 + self.look_up_down_differential * 4) + half_lid_spacing
        ]
        self.opened_lids = self.lids

if __name__ == "__main__":
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(AnotherEye(ApplicationContext()))
